#include "networkhandler.h"

NetworkHandler::NetworkHandler(QHostAddress remoteHost, qint16 port, QObject *parent)
    : QObject(parent)
    , remoteHost(remoteHost)
    , port(port)
{
}

QSharedPointer<QUdpSocket> NetworkHandler::getSocket()
{
    if (socket.isNull())
    {
        socket.reset(new QUdpSocket(this));
        socket->bind(QHostAddress::Any, getPort());
    }
    return socket;
}

const QHostAddress& NetworkHandler::getRemoteHost()
{
    return remoteHost;
}

void NetworkHandler::setRemoteHost(QHostAddress host)
{
    remoteHost = host;
}

qint16 NetworkHandler::getPort()
{
    return port;
}

void NetworkHandler::setPort(qint16 port)
{
    this->port = port;
    getSocket()->bind(QHostAddress::Any, port);
}

QByteArray NetworkHandler::readDatagram()
{
    QByteArray datagram;
    if (getSocket()->hasPendingDatagrams())
    {
        datagram.resize(getSocket()->pendingDatagramSize());
        getSocket()->readDatagram(datagram.data(), datagram.size());
    }
    return datagram;
}

void NetworkHandler::writeDatagram(const QByteArray& datagram)
{
    if (datagram.size() > 0) {
        getSocket()->writeDatagram(datagram, getRemoteHost(), getPort());
    }
}
