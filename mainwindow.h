#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSharedPointer>
#include "audiohandler.h"
#include "networkhandler.h"
#include "audioreceiver.h"
#include "audiosender.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    static const int port = 5054;
    static const int audioBufferSize = 16384;

    QSharedPointer<AudioHandler> audioHandler;
    QSharedPointer<NetworkHandler> networkHandler;
    QSharedPointer<AudioReceiver> audioReceiver;
    QSharedPointer<AudioSender> audioSender;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btnStartStreaming_clicked();

    void on_spinBoxInterval_valueChanged(int arg1);

    void on_horizontalSliderVoume_sliderMoved(int position);

    void on_spinBoxBuffer_valueChanged(int arg1);

    void on_checkBoLowPass_clicked(bool checked);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
