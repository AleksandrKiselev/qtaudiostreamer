#ifndef NETWORKHANDLER_H
#define NETWORKHANDLER_H

#include <QObject>
#include <QSharedPointer>
#include <QUdpSocket>
#include <QHostAddress>

class NetworkHandler : public QObject
{
    Q_OBJECT
private:
    QSharedPointer<QUdpSocket> socket;
    QHostAddress remoteHost;
    qint16 port;

public:
    explicit NetworkHandler(QHostAddress remoteHost, qint16 port, QObject *parent = 0);

    QSharedPointer<QUdpSocket> getSocket();

    const QHostAddress& getRemoteHost();
    void setRemoteHost(QHostAddress host);

    qint16 getPort();
    void setPort(qint16 port);

    QByteArray readDatagram();
    void writeDatagram(const QByteArray& datagram);
};

#endif // NETWORKHANDLER_H
