#-------------------------------------------------
#
# Project created by QtCreator 2016-04-07T08:11:30
#
#-------------------------------------------------

QT       += core gui multimedia network

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtAudioStreamer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    audiohandler.cpp \
    audiosender.cpp \
    audioreceiver.cpp \
    networkhandler.cpp \
    ima.cpp

HEADERS  += mainwindow.h \
    audiohandler.h \
    audiosender.h \
    audioreceiver.h \
    networkhandler.h \
    ima.h

FORMS    += mainwindow.ui
