#ifndef AUDIOHANDLER_H
#define AUDIOHANDLER_H

#include <QObject>
#include <QSharedPointer>
#include <QByteArray>
#include <QAudioFormat>
#include <QAudioOutput>
#include <QAudioInput>
#include <QIODevice>
#include <QUdpSocket>
#include <QTimer>
#include "ima.h"

class AudioHandler : public QObject
{
    Q_OBJECT
private:
    QSharedPointer<QAudioOutput> audioOut;
    QSharedPointer<QAudioInput> audioIn;
    QIODevice* inDevice;
    QIODevice* outDevice;
    int bufferSize;
    int volume;
    bool lowPass;

    ima_state stateDecode[2];
    ima_state stateEncode[2];

private:
    void init();
    QByteArray encode(const QByteArray& data);
    QByteArray decode(const QByteArray& data);

public:
    explicit AudioHandler(int bufferSize, QObject *parent = 0);

    QByteArray read(int readlen);
    int write(const QByteArray& data);

    QIODevice* getInDevice();
    QIODevice* getOutDevice();
    QAudioOutput* getOut();
    QAudioInput* getIn();

    void setVolume(int volume);
    void setLowPass(bool lowPass);

public:
    static bool formatInited;
    static const int sampleSize;
    static const int sampleRate;
    static const int channelCount;
    static const QAudioFormat& getFormat();

    static int getSampleSize(int msec);
};

#endif // AUDIOHANDLER_H
