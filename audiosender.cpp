#include "audiosender.h"

AudioSender::AudioSender(QSharedPointer<AudioHandler> haudio, QSharedPointer<NetworkHandler> hnet,
                         int interval, QObject *parent)
    : QObject(parent)
    , haudio(haudio)
    , hnet(hnet)
    , interval(interval)
    , readlen(0)
{
}

bool AudioSender::start()
{
    if (!hnet.isNull() && !haudio.isNull() && interval > 0)
    {
        setInterval(interval);

        timer.reset(new QTimer(this));
        connect(timer.data(), SIGNAL(timeout()),this, SLOT(writeDatagrams()));
        timer->start(interval);

        return true;
    }
    return false;
}

void AudioSender::setInterval(int interval)
{
    readlen = haudio->getSampleSize(interval);
    if (!timer.isNull())
        timer->setInterval(interval);
}

void AudioSender::writeDatagrams()
{
    if (hnet.isNull() || haudio.isNull())
        return;

    QByteArray payload = haudio->read(readlen);
    int size = payload.size();
    if (size > 0) {
        hnet->writeDatagram(payload);
    }
}
