#include "audioreceiver.h"

AudioReceiver::AudioReceiver(QSharedPointer<AudioHandler> haudio, QSharedPointer<NetworkHandler> hnet,
                             int bufferTime, QObject *parent)
    : QObject(parent)
    , haudio(haudio)
    , hnet(hnet)
    , bufferTime(bufferTime)
    , bufferSize(0)
    , playCalled(false)
{
}

void AudioReceiver::start()
{
    if (hnet.isNull() || haudio.isNull())
        return;

    bufferSize = haudio->getSampleSize(bufferTime);

    connect(hnet->getSocket().data(), SIGNAL(readyRead()), this, SLOT(readDatagrams()));
}

void AudioReceiver::readDatagrams()
{
    if (hnet.isNull() || haudio.isNull())
        return;

    QByteArray payload = hnet->readDatagram();
    if (payload.size() > 0) {
        emit addToBuffer(payload);
    }
}

void AudioReceiver::addToBuffer(QByteArray data)
{
    buffer.append(data);
    if (!playCalled)
    {
        playCalled = true;
        QMetaObject::invokeMethod(this, "playBuffer", Qt::QueuedConnection);
    }
}

void AudioReceiver::playBuffer()
{
    playCalled = false;
    if (hnet.isNull() || haudio.isNull()) return;
    if (buffer.isEmpty() || buffer.size() < bufferSize)
        return;

    const QAudioOutput* audio = haudio->getOut();
    while (!buffer.isEmpty())
    {
        int readlen = audio->periodSize();
        if (readlen <= 0) break;
        int chunks = audio->bytesFree() / readlen;
        while (chunks)
        {
            QByteArray buffToPlay = buffer.mid(0, readlen);
            if (int len = buffToPlay.size())
            {
                buffer.remove(0, len);
                haudio->write(buffToPlay);
            }
            chunks--;
        }
    }
}

void AudioReceiver::setBufferTime(int time)
{
    bufferTime = time;
    bufferSize = haudio->getSampleSize(time);
}

