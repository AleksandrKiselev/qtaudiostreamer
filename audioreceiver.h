#ifndef AUDIORECEIVER_H
#define AUDIORECEIVER_H

#include "audiohandler.h"
#include "networkhandler.h"
#include <QObject>
#include <QSharedPointer>
#include <QIODevice>
#include <QByteArray>

class AudioReceiver : public QObject
{
    Q_OBJECT
private:
    QSharedPointer<AudioHandler> haudio;
    QSharedPointer<NetworkHandler> hnet;
    QByteArray buffer;
    int bufferTime; // buffer time (msec)
    int bufferSize;
    bool playCalled;

public:
    explicit AudioReceiver(QSharedPointer<AudioHandler> haudio, QSharedPointer<NetworkHandler> hnet,
                           int bufferTime, QObject *parent = 0);
    void start();
    void setBufferTime(int time);


private slots:
    void readDatagrams();
    void playBuffer();

private:
    void addToBuffer(QByteArray data);
};

#endif // AUDIORECEIVER_H
