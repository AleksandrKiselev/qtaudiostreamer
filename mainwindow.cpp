#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QNetworkInterface>
#include <QNetworkAddressEntry>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->listWidgetIp->addItem("All Local Addresses");
    QList<QHostAddress> list = QNetworkInterface::allAddresses();
    for(int nIter = 0; nIter < list.count(); nIter++)
    {
        if(!list[nIter].isLoopback())
        if (list[nIter].protocol() == QAbstractSocket::IPv4Protocol )
        ui->listWidgetIp->addItem(list[nIter].toString());
    }

    audioHandler.reset(new AudioHandler(audioBufferSize, this));
    audioHandler->setVolume(ui->horizontalSliderVoume->value());
    audioHandler->setLowPass(ui->checkBoLowPass->checkState() == Qt::CheckState::Checked);

    networkHandler.reset(new NetworkHandler(QHostAddress::Null, port, this));

    audioReceiver.reset(new AudioReceiver(audioHandler, networkHandler, ui->spinBoxBuffer->value(), this));
    audioReceiver->start();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnStartStreaming_clicked()
{
    networkHandler->setRemoteHost(QHostAddress(ui->leHost->text()));
    audioSender.reset(new AudioSender(audioHandler, networkHandler, ui->spinBoxInterval->value(), this));
    audioSender->start();
}

void MainWindow::on_spinBoxInterval_valueChanged(int arg1)
{
    if (!audioSender.isNull())
        audioSender->setInterval(arg1);
}

void MainWindow::on_horizontalSliderVoume_sliderMoved(int position)
{
    if (!audioHandler.isNull())
        audioHandler->setVolume(position);
}

void MainWindow::on_spinBoxBuffer_valueChanged(int arg1)
{
    if (!audioReceiver.isNull())
        audioReceiver->setBufferTime(arg1);
}

void MainWindow::on_checkBoLowPass_clicked(bool checked)
{
    audioHandler->setLowPass(checked);
}
