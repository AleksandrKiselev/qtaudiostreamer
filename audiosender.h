#ifndef AUDIOSENDER_H
#define AUDIOSENDER_H

#include "audiohandler.h"
#include "networkhandler.h"
#include <QObject>
#include <QHostAddress>
#include <QSharedPointer>
#include <QAudioInput>

class AudioSender : public QObject
{
    Q_OBJECT
protected:
    QSharedPointer<AudioHandler> haudio;
    QSharedPointer<NetworkHandler> hnet;
    QSharedPointer<QTimer> timer;
    int interval; // msec
    int readlen;

public:
    explicit AudioSender(QSharedPointer<AudioHandler> haudio, QSharedPointer<NetworkHandler> hnet,
                         int interval, QObject *parent = 0);
    bool start();
    void setInterval(int interval);

private slots:
    void writeDatagrams();
};

#endif // AUDIOSENDER_H
