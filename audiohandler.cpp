#include "audiohandler.h"

AudioHandler::AudioHandler(int bufferSize, QObject *parent)
    : QObject(parent)
    , inDevice(nullptr)
    , outDevice(nullptr)
    , bufferSize(bufferSize)
    , volume(1000)
    , lowPass(true)

{
    memset(&stateDecode, 0, sizeof(stateDecode));
    memset(&stateEncode, 0, sizeof(stateEncode));
}

QByteArray AudioHandler::encode(const QByteArray& data)
{
    QByteArray encodedData;
    if (int size = data.size())
    {
        encodedData.resize(size / 4);
        ima_encode_mono(stateEncode, (void*)encodedData.data(), (void*)data.data(), size);
    }
    return encodedData;
}

QByteArray AudioHandler::decode(const QByteArray& data)
{
    QByteArray decodedData;
    if (int size = data.size())
    {
        decodedData.resize(size * 4);
        ima_decode_mono(stateDecode, (void*)decodedData.data(), (void*)data.data(), size);
    }
    return decodedData;
}

QByteArray AudioHandler::read(int readlen)
{
    QByteArray readedData;
    QIODevice* device = getInDevice();
    if (device && readlen > 0)
    {
        readedData = encode(device->read(readlen));
    }
    return readedData;
}

int AudioHandler::write(const QByteArray& data)
{
    int result = 0;
    QIODevice* device = getOutDevice();

    if (device)
    {
        QByteArray decodedData = decode(data);
        int writelen = decodedData.size();

        int min = std::numeric_limits<qint16>::min();
        int max = std::numeric_limits<qint16>::max();

        qint16 *samples = (qint16*)decodedData.data();
        int size = writelen / sizeof(qint16);
        for (int i = 0; i < size; i++)
        {
            int sample = (qint16)samples[i];
            sample = sample * (volume / (double)100);
            sample = qBound(min, sample, max);
            if (i == 0 || !lowPass) {
                samples[i] = sample;
            } else {
                //Remove noise using Low Pass filter algortm[Simple algorithm used to remove noise]
                samples[i] = 0.333333 * sample + ( 1.0 - 0.333333 ) * samples[i - 1];
            }
        }

        result = device->write(decodedData);
    }
    return result;
}

void AudioHandler::init()
{
    if (audioOut.isNull())
    {
        audioOut.reset(new QAudioOutput(getFormat(), this));
        audioOut->setBufferSize(bufferSize);
        outDevice = getOut()->start();
        if (!outDevice) {
            qDebug() << "Can't get output audio device";
        }
    }

    if (audioIn.isNull())
    {
        audioIn.reset(new QAudioInput(getFormat(), this));
        audioIn->setBufferSize(bufferSize);
        inDevice = audioIn->start();
        if (!inDevice) {
            qDebug() << "Can't get input audio device";
        }
    }
}

QAudioOutput* AudioHandler::getOut()
{
    if (audioOut.isNull()) init();
    return audioOut.data();
}

QAudioInput* AudioHandler::getIn()
{
    if (audioIn.isNull()) init();
    return audioIn.data();
}

QIODevice* AudioHandler::getOutDevice()
{
    if (!outDevice) init();
    return outDevice;
}

QIODevice* AudioHandler::getInDevice()
{
    if (!inDevice) init();
    return inDevice;
}

void AudioHandler::setVolume(int volume)
{
    this->volume = volume;
}

void AudioHandler::setLowPass(bool lowPass)
{
    this->lowPass = lowPass;
}

// Format
bool AudioHandler::formatInited = false;
const int AudioHandler::sampleSize = 16;
const int AudioHandler::sampleRate = 8000;
const int AudioHandler::channelCount = 1;
const QAudioFormat& AudioHandler::getFormat()
{
    static QAudioFormat format;
    if (!formatInited) {
        formatInited = true;
        format.setSampleRate(sampleRate);
        format.setChannelCount(channelCount);
        format.setSampleSize(sampleSize);
        format.setCodec("audio/pcm");
        format.setByteOrder(QAudioFormat::LittleEndian);
        format.setSampleType(QAudioFormat::UnSignedInt);
    }
    return format;
}

int AudioHandler::getSampleSize(int msec)
{
    if (msec == 0)
        return 0;
    const QAudioFormat& f = getFormat();
    return f.channelCount() * (f.sampleSize() / 8) * f.sampleRate() / (double)1000 * msec;
}

